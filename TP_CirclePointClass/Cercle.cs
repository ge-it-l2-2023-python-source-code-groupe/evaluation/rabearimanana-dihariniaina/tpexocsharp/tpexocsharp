using System;
using ClassPoint;

namespace ClassCercle;

public partial class Cercle{
// Définition des attributs Centre ( à appeler) et Rayon du cercle 
    public Point point {get; set;}
    public double Rayon {get; set;}
    // public float Pi = 3.14;


// Constructeur vide 
public Cercle(){
    
}
//  Constructeur 
public Cercle(Point pt, double rayon){
    point = pt;
    Rayon = rayon;
}

// Méthode getPerimeter()
public double getPerimeter(){
    double perimeter = Math.PI*2*Rayon; 
    // Format avec deux décimales
    return Math.Round(perimeter, 2);
}

//  Méthode getSurface()
public double getSurface(){
    return Math.Round((Math.PI*Rayon*Rayon),2);   
}

//Méthode IsIncluding(Point P) pour retourner si le point P appartient au cercle
public void IsIncluding(Point P){
//  Calcul de distance : 
    double Distance = Math.Sqrt((P.X - point.X)*(P.X - point.X) + (P.Y - point.Y)*(P.Y - point.Y));

// Condition d'appartenance 
    if (Distance <= Rayon ){
        Console.WriteLine("Le point appartient au cercle");
    }
    else{
        Console.WriteLine("Le point n'appartient pas au cercle");
    }
}

//  Méthode Display() qui affiche une chaine de caractères CERCLE (x,y)
public void Display(){
    Console.WriteLine($"CERCLE({point.X}, {point.Y}, {Rayon})");
}
}