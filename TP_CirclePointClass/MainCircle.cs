using System;
using ClassCercle;
using ClassPoint;

namespace PrincipalCircle;

class Circle
{
// Main 
    public static void MainCircle()
    {

    // Obtention des informations de l'User: 

    // L'abscisse du centre
    bool a = false;
    int x = 0, y = 0, R = 0, X = 0, Y = 0;
    
    while(!a) {
        Console.WriteLine("Donner l'abscisse du centre:");
        a = int.TryParse(Console.ReadLine()??"", out x);
        if(!a){
            Console.WriteLine("Invalid input!!!");
        }
    }

    a = false;
    while(!a){
        Console.WriteLine ("Donner l'ordonné du centre:");
        a = int.TryParse(Console.ReadLine()??"", out y);
        if(!a){
            Console.WriteLine("Invalid input!!!");
        }
    }

    a = false;
    while(!a){
        Console.WriteLine ("Donner le rayon:");
        a = int.TryParse(Console.ReadLine()??"", out R);
        if(!a){
            Console.WriteLine("Invalid input!!!");
        }
    }
    
    Point Pt1  = new Point (x,y);

    Cercle C1 = new Cercle(Pt1,R);
    // affichage du CERCLE (x,y,Rayon)
    C1.Display();
    Console.WriteLine ("Le périmètre est:" + C1.getPerimeter());
    Console.WriteLine ("La surface est:" + C1.getSurface());

    // Donner un point
    Console.WriteLine("Donner un point:");
    
    a = false;
    while(!a){
        Console.WriteLine ("X:");
        a = int.TryParse(Console.ReadLine()??"", out X);
        if(!a){
            Console.WriteLine("Invalid input!!!");
        }
    }
    a = false;
    while(!a){
        Console.WriteLine ("Y:");
        a = int.TryParse(Console.ReadLine()??"", out Y);
        if(!a){
            Console.WriteLine("Invalid input!!!");
        }
    }

    Point Pt2 = new Point(X,Y);
    C1.IsIncluding(Pt2); 

    Console.ReadKey();
    }
}
