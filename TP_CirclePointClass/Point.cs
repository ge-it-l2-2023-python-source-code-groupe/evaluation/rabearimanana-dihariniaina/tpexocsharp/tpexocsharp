using System;

namespace ClassPoint;

public partial class Point{
// Définition des attributs x et y de type réel 
public double X {get; set;}
public double Y {get; set;}

// Constructeur vide 
public Point(){
    
}
//  Définition de notre  constructeur pour définir des valeurs de x et y 
public Point(double x, double y){
    X = x;
    Y = y;
}

//  Méthode Display() qui affiche une chaine de caractères POINT (x,y)
public void Display(){
    // Console.WriteLine("Le point x:" + x);
    // Console.WriteLine("Le point y:" + y);
    Console.WriteLine($"X: {X} \n Y: {Y} \n POINT({X}, {Y})");
}

}
