namespace JeuDeDes;

        // Les faces de dés de 1 à 6
partial class Lancer{
    public static void LancerDe(Serie ActualSerie) {
            Random random = new Random();
            int randomNumber = random.Next(1, 7);

            if (randomNumber == 1) {
                Console.WriteLine("|     |\n|  *  |\n|     |");
                ActualSerie.FacesPlayer += randomNumber + ";";
            } else if (randomNumber == 2) {
                Console.WriteLine("|*    |\n|     |\n|    *|");
                ActualSerie.FacesPlayer += randomNumber + ";";
            } else if (randomNumber == 3) {
                Console.WriteLine("|*    |\n|  *  |\n|    *|");
                ActualSerie.FacesPlayer += randomNumber + ";";
            } else if (randomNumber == 4) {
                Console.WriteLine("|*   *|\n|     |\n|*   *|");
                ActualSerie.FacesPlayer += randomNumber + ";";
            } else if (randomNumber == 5) {
                Console.WriteLine("|*   *|\n|  *  |\n|*   *|");
                ActualSerie.FacesPlayer += randomNumber + ";";
            } else if (randomNumber == 6) {
                Console.WriteLine("|*   *|\n|*   *|\n|*   *|");
                ActualSerie.FacesPlayer += randomNumber + ";";
                ActualSerie.NbMaxPlayer++;
            }
        }
}
