namespace JeuDeDes;

    //  Main pour lancer le jeu 
    static partial class Player {
        public static void NouveauJoueur() {
            nbPlayer = 0;
            PlayerList = "";
            PlayerScore = "";
            PlayerPoint = "";

            bool a = false;    
            while(!a) {
                Console.WriteLine("Entrez le nombre de joueur :");
                a = int.TryParse(Console.ReadLine()??"", out nbPlayer);
            if(!a){
                Console.WriteLine("Invalid input!!!");
                }
            }

            for (int i = 1; i <= nbPlayer; i++) {
                Console.Write($"\nEntrez le nom du joueur {i} : ");
                string name = Console.ReadLine() ?? "aa";

                PlayerList += name + ";";
                PlayerScore += 0 + ";";
                PlayerPoint += 0 + ";";
            }
 
            Console.WriteLine("\nLes joueurs sont : ");

            for (int i = 0; i < nbPlayer; i++) {
                Console.WriteLine($"- {PlayerList.Split(";")[i]} : 0");
            }
        }

        // Manche pour lancer le dés 
        public static void Manche() {
            Serie ActualSerie = new Serie();

            ActualSerie.ListPlayerSerie = PlayerList;

            for (int i = 0; i < nbPlayer; i++) {
                Console.WriteLine($" \nC'est le tour de {PlayerList.Split(";")[i]} ");
                bool isValid = false;

                string  choice ="";
                while (!isValid){
                    Console.WriteLine("Voulez vous lancer le dé ? (y/n) ");
                    Console.Write("::: ");
                    choice = Console.ReadLine()?? "";
                    if (choice.Length > 0 && choice.ToLower()[0] == 'y'){
                        isValid = true;
                    }else if(choice.Length > 0 && choice.ToLower()[0]== 'n'){
                        isValid = true;
                    }else{
                        Console.WriteLine("Saisie invalide. Veuillez entrer 'Y' pour oui ou 'N' pour non.");
                    }
                }
                    
                        switch (choice.ToLower()[0]) {
                            case 'y':
                                Lancer.LancerDe(ActualSerie);
                                
                                break;
                            case 'n':
                                ActualSerie.FacesPlayer += "1;";
                                Console.WriteLine("------ Vous avez passé votre tour ------\n");
                                
                                break;
                            
                            default:
                                ActualSerie.FacesPlayer += "1";
                                break;
                        }
                    

            }

            PlayerScore = AjouterScores(ActualSerie);
            ActualSerie.ScrorePlayerSerie = PlayerScore;

            PlayerPoint = AjouterPoints(ActualSerie);
            ActualSerie.PointPlayerSerie = PlayerPoint;

            ActualSerie.AjouterHistorique();

            Console.WriteLine("\nPoints des joueurs :");

            for (int i = 0; i < nbPlayer; i++) {
                Console.WriteLine($"- {PlayerList.Split(";")[i]} : {PlayerPoint.Split(";")[i]}");
            }

            Console.WriteLine("\nScores des joueurs :");

            for (int i = 0; i < nbPlayer; i++) {
                Console.WriteLine($"- {PlayerList.Split(";")[i]} : {PlayerScore.Split(";")[i]}");
            }
        }

        // Conditions pour les points

        static string AjouterPoints(Serie ActualSerie) {
            string tmpListePoints = "";

            for(int i = 0; i < ActualSerie.FacesPlayer.Split(";").Length - 1; i++) {
                if (ActualSerie.FacesPlayer.Split(";")[i] == "6" && ActualSerie.NbMaxPlayer == 1) {
                    tmpListePoints += Convert.ToString(int.Parse(PlayerPoint.Split(";")[i]) + 2) + ";";
                } else if (ActualSerie.FacesPlayer.Split(";")[i] == "6" && ActualSerie.NbMaxPlayer > 1) {
                    tmpListePoints += Convert.ToString(int.Parse(PlayerPoint.Split(";")[i]) + 1) + ";";
                } else {
                    tmpListePoints += Convert.ToString(int.Parse(PlayerPoint.Split(";")[i]) + 0) + ";";
                }
            }
            return tmpListePoints;
        }

        // Conditions pour les Scores
        static string AjouterScores(Serie ActualSerie) {
            string tmpListeScores = "";

            for(int i = 0; i < ActualSerie.FacesPlayer.Split(";").Length - 1; i++) {
                if (ActualSerie.FacesPlayer.Split(";")[i] == "6") {
                    tmpListeScores += Convert.ToString(int.Parse(PlayerScore.Split(";")[i]) + 1) + ";";
                } else {
                    tmpListeScores += Convert.ToString(int.Parse(PlayerScore.Split(";")[i]) + 0) + ";";
                }
            }
            return tmpListeScores;
        }
    }
