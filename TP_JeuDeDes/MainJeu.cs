namespace JeuDeDes;

    class MainJeu {
        public static void JeuDeDes() {
            Console.WriteLine("Bienvenue sur ce jeu de dés!\n");

            Player.NouveauJoueur();
            bool jouer = true;
            while (jouer) {
                Console.WriteLine(@"
1) Nouvelle manche 
2) Voir l'historique des manches 
3) Quitter le programme ");

                Console.Write("\nVotre choix : ");
                string choix = Console.ReadLine() ?? "";

                switch (choix) {
                    case "1":
                        Console.WriteLine("\nNouvelle manche!");
                        Player.Manche();
                        Console.ReadKey();
                        break;
                    case "2":
                        Console.WriteLine("\nHistorique des manches:");
                        if (Serie.HistoriqueSeries.Equals(""))
                            Console.WriteLine("\nIl n'y a pas d'historique à afficher.");
                        else
                            Serie.AfficherHistorique();
                        Console.ReadKey();
                        break;
                    case "3":
                        Console.WriteLine("\nVous quittez le programme");
                        Console.ReadKey();
                        jouer = false;
                        break;
                    default:
                        Console.WriteLine("Il y a une erreur!!!!! ");
                        break;
                }
            }
        }
    }

















