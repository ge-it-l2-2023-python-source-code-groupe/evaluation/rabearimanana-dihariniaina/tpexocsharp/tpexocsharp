namespace JeuDeDes;

    // Initialisation des attributs Series
    partial class Serie {
        public int NumSerie { get; private set; }
        private static int num = 0;
        public DateTime DateSerie { get; set; }
        public string ListPlayerSerie { get; set; }
        public string ScrorePlayerSerie { get; set; }
        public string PointPlayerSerie { get; set; }
        public string FacesPlayer { get; set; }
        public int NbMaxPlayer { get; set; }
        public static string HistoriqueSeries = "";

        public Serie() {
            NumSerie = ++num;
            DateSerie = DateTime.Now;
            ListPlayerSerie = "";
            ScrorePlayerSerie = "";
            PointPlayerSerie = "";
            FacesPlayer = "";
            NbMaxPlayer = 0;
        }
    }

