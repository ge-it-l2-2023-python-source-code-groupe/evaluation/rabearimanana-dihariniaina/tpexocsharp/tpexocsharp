namespace JeuDeDes;

    // Initialisation des méthodes Series

    partial class Serie {
        public void AjouterHistorique() {
            HistoriqueSeries += FormatageHistorique() + "\n";
        }

        public static void AfficherHistorique() {
            Console.WriteLine(HistoriqueSeries);
        }

        private string FormatageHistorique() {
            return $@"
Manche N° {NumSerie}

Date : {DateSerie}

Points des joueurs :
{FormatageScoresPoints(PointPlayerSerie)}

Scores des joueurs : 
{FormatageScoresPoints(ScrorePlayerSerie)}";
        }

        public string FormatageScoresPoints(string listeScoresPoints) {
            string scoresPointsFormates = "";
            for (int i = 0; i < ListPlayerSerie.Split(";").Length - 1; i++) {
                scoresPointsFormates += $"\n- {ListPlayerSerie.Split(";")[i]} : {listeScoresPoints.Split(";")[i]}";
            }
            return scoresPointsFormates;
        }
    }
