﻿using System;
using DisplayBanque;
using PrincipalCircle;
using JeuDeDes;
using HorlogeModule;
namespace TpExoCsharp;


class Program
{
    public static void AfficherMenu()
    {
        Console.Clear();
        Console.WriteLine("""
        BIENVENUE SUR MON APPLICATION DE TP \n
        ~~~By RABEARIMANANA Dihariniaina~~~

        ***********************************
        """);
        Console.WriteLine("Menu principal :");
        Console.WriteLine("1. TP 1 - Client et Compte");
        Console.WriteLine("2. TP 2 - Cercle et Point");
        Console.WriteLine("3. TP 3 - Jeu de Dés");
        Console.WriteLine("4. TP 4 - Horloge Module");
        Console.WriteLine("5. Quitter");
        Console.Write("Choix : ");
    }
    public static void Main(String[] args){

        
        //TP1.ExecuterTP1();
        
            bool quitter = false;

            do
            {
                AfficherMenu();

                // Lire le choix de l'utilisateur
                string choix = Console.ReadLine();

                // Exécuter l'action correspondante
                switch (choix)
                {
                    case "1":
                        Display.MainBanque(); // TP_Banque
                        Console.ReadKey();
                        break;

                    case "2":
                        Circle.MainCircle(); // TP_Cercle
                        Console.ReadKey();
                        break;
                    case "3":
                        MainJeu.JeuDeDes(); // TP_JeuDe
                        Console.ReadKey();
                        break;
                    case "4":
                        MainWatch.HorlogeMain();// TP_Horloge
                        Console.ReadKey();
                        break;

                    case "5":
                        Console.WriteLine("""
                        :::::::::::::::::::::MERCI D'AVOIR JOUER::::::::::::::::::::::::::::::
                        """);
                        quitter = true;
                        break;

                    default:
                        Console.WriteLine("Choix invalide. Appuyez sur entrer et veuillez réessayer.");
                        Console.ReadKey();
                        break;
                }

            } while (!quitter);
        
    }
}