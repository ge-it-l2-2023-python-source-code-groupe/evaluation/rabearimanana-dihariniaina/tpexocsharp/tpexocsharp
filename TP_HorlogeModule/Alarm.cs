using System;
using System.Collections.Generic;

namespace HorlogeModule
{
    class alarmApp
    {
        private static List<string> alarmList = new List<string>();

        public static void ShowAlarmMenu()
        {
            bool continueAlarmMenu = true;

            while (continueAlarmMenu)
            {
                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Voir les Alarmes actives");
                Console.WriteLine("2 - Créer une alarme");
                Console.WriteLine("3 - Retour au menu principal");

                Console.Write("Quel est votre choix ? ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        DisplayAlarms();
                        break;
                    case "2":
                        CreateAlarm();
                        break;
                    case "3":
                        continueAlarmMenu = false;
                        break;
                    default:
                        Console.WriteLine("Choix invalide. Veuillez choisir une option valide.");
                        break;
                }
            }
        }

        private static void DisplayAlarms()
        {
            Console.WriteLine("Liste d’alarmes actives :");
            foreach (string alarm in alarmList)
            {
                Console.WriteLine(alarm);
            }
        }

        private static void CreateAlarm()
        {
            Console.WriteLine("Info de la nouvelle Alarme :\n");
            Console.Write("Donnez un nom de référence : ");
            string alarmName = Console.ReadLine();

            Console.Write("Heure de l’alarme (hh:mm) : ");
            string alarmTime = Console.ReadLine();

            Console.Write("Date de planification (L/M/ME/J/V/S/D) : ");
            string alarmDate = Console.ReadLine();

            Console.Write("Lancer une seule fois ? (y/n) : ");
            bool launchOnce = Console.ReadLine()?.Equals("y", StringComparison.OrdinalIgnoreCase) ?? false;

            Console.Write("Périodique ? (y/n) : ");
            bool isPeriodic = Console.ReadLine()?.Equals("y", StringComparison.OrdinalIgnoreCase) ?? false;

            Console.Write("Activer sonnerie par defaut ? (y/n) : ");
            bool activateDefaultRingtone = Console.ReadLine()?.Equals("y", StringComparison.OrdinalIgnoreCase) ?? false;

            string newAlarm = $"{alarmName} - {alarmTime} - {alarmDate} - Lancer une seule fois : {(launchOnce ? "Oui" : "Non")} - Périodique : {(isPeriodic ? "Oui" : "Non")} - Activer sonnerie par défaut : {(activateDefaultRingtone ? "Oui" : "Non")}";
            alarmList.Add(newAlarm);

            Console.WriteLine("Votre nouvelle alarme est enregistrée.");
        }
    }
}
