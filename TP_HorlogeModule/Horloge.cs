using System;
using System.Collections.Generic;

namespace HorlogeModule
{
    class horlogeApp
    { 
        // Prendre toutes les fuseaux horaires
        public static IList<TimeZoneInfo> TimeZoneList = TimeZoneInfo.GetSystemTimeZones();
        public static List<TimeZoneInfo> TZList = new List<TimeZoneInfo> {};

        // Méthode pour rechercher les fuseaux horaires :
        public static List<TimeZoneInfo> SearchSuggestion (string searchString){
            List<TimeZoneInfo> correspondence = new List<TimeZoneInfo>{};
            foreach (TimeZoneInfo item in TimeZoneList)
            {
                if (item.DisplayName.ToLower().Contains(searchString.ToLower())){
                    correspondence.Add(item);
                }
            }
            return correspondence;
        }

        private static List<string> clockList = new List<string>();

        public static void ShowClockMenu()
        {
            bool continueClockMenu = true;

            while (continueClockMenu)
            {
                Console.WriteLine("Choisissez une option :");
                Console.WriteLine("1 - Voir les Horloges actives");
                Console.WriteLine("2 - Ajouter une Horloge");
                Console.WriteLine("3 - Retour au menu principal");

                Console.Write("Quel est votre choix ? ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        DisplayClocks();
                        break;
                    case "2":
                        AddClock();
                        break;
                    case "3":
                        continueClockMenu = false;
                        break;
                    default:
                        Console.WriteLine("Choix invalide. Veuillez choisir une option valide.");
                        break;
                }
            }
        }

        private static void DisplayClocks()
        {
            Console.WriteLine("Liste Horloges :");
            foreach (string clock in clockList)
            {
                Console.WriteLine(clock);
            }
        }

        private static void AddClock()
        {
            string city;
            List<TimeZoneInfo> Suggestions; 
            while(true){
            Console.WriteLine("Choisissez une ville parmi [Moscou, Dubai, Mexique] : ");
            city = Console.ReadLine();

            // Suggestion pour les fuseaux horaires: 
            Suggestions = SearchSuggestion(city);

            if (Suggestions.Count >1){
                Console.WriteLine("Suggestions:");
                foreach (TimeZoneInfo Suggestion in Suggestions){
                    Console.WriteLine(Suggestion.DisplayName);
                }
            }
            else if (Suggestions.Count == 0){
                Console.WriteLine("Aucune suggestion trouvée");
            }
            else if (!TZList.Contains(Suggestions[0])){
                Console.WriteLine(Suggestions[0].DisplayName);
                TZList.Add(Suggestions[0]);
                break;
            }
            else {
                Console.WriteLine("L'horloge est déjà actif");
            }

        }
            

            string newClock = $"{city} - {TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, Suggestions[0])}";
            clockList.Add(newClock);

            Console.WriteLine("Votre Horloge a bien été enregistrée.");
        }
    }
}
