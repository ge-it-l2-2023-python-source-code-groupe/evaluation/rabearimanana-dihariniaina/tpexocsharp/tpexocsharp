using System;


namespace HorlogeModule;

class MainWatch
{
    public static void HorlogeMain()
    {
        alarmApp alarm= new alarmApp();
        horlogeApp holorge = new horlogeApp();

        bool continueApp = true;

// Affichage du menu et des choix: 

        while (continueApp)
        {
            Console.WriteLine("Bienvenue dans le menu principal (*v*)");
            Console.WriteLine("Choisissez une option :");
            Console.WriteLine("1 - Alarme");
            Console.WriteLine("2 - Horloge");
            Console.WriteLine("3 - Quitter");

            Console.Write("Quel est votre choix ? ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    alarmApp.ShowAlarmMenu();
                    break;
                case "2":
                    horlogeApp.ShowClockMenu();
                    break;
                case "3":
                    continueApp = false;
                    break;
                default:
                    Console.WriteLine("Choix invalide. Veuillez choisir une option valide.");
                    break;
            }
        }
    }
}
